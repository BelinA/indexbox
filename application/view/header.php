<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Test task</title>
        <link href="/application/view/css/bootstrap.css" rel="stylesheet" type='text/css'>
        <link href="/application/view/css/style.css" rel="stylesheet" type='text/css'>
        <link rel="stylesheet" href="/application/view/font-awesome/css/font-awesome.min.css">
        <script type="text/javascript" src="/application/view/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="/application/view/js/popper.min.js"></script>
        <script type="text/javascript" src="/application/view/js/bootstrap.js"></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">