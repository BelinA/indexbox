<div class="col-md-3 col-sm-12">
    <h4>Фильтры</h4>
    <form>
        <div class="form-group">
            <label for="viewsCount">Сортировать по кол-ву просмотров</label>
            <select id="viewsCount" class="form-control">
                <option selected>Выберите из списка</option>
                <option>По возрастанию</option>
                <option>По убыванию</option>
            </select>
        </div>
        <div class="form-group">
            <label for="productName">Сортировать по продукту</label>
            <select id="productName" class="form-control">
                <option selected>Выберите из списка</option>
                <option>По возрастанию</option>
                <option>По убыванию</option>
            </select>
        </div>
        <div class="form-group">
            <label for="addDate">Сортировать по дате добавления</label>
            <select id="addDate" class="form-control">
                <option selected>Выберите из списка</option>
                <option>По возрастанию</option>
                <option>По убыванию</option>
            </select>
        </div>
        <div class="form-group">
            <label for="numColumns">Количество выводимых статей</label>
            <select id="numColumns" class="form-control">
                <option selected>Выберите из списка</option>
                <option>1</option>
                <option>3</option>
                <option>5</option>
                <option>10</option>
                <option>20</option>
            </select>
        </div>
        <div class="text-center m-1">
            <button type="submit" class="btn btn-primary">Применить</button>
        </div>
    </form>
</div>
