<?
function class_autoload($class_name) {
  $file = SYS.'class/'.strtolower($class_name).'.php';
  //echo $file.'<br>';
  if(file_exists($file) == false){
      return false;
  }
  require_once($file);
}

function controller_autoload($class_name) {
  $file = APP.'controller/'.strtolower($class_name).'.php';
  //echo $file.'<br>';
  if(file_exists($file) == false){
    return false;
  }
  require_once($file);
}

function model_autoload($class_name) {
  $file = APP.'model/'.strtolower($class_name).'.php';
  //echo $file.'<br>';
  if(file_exists($file) == false){
    return false;
  }
  require_once($file);
}

spl_autoload_register('class_autoload');
spl_autoload_register('controller_autoload');
spl_autoload_register('model_autoload');
