<?
class Controller extends Single{
    function __call($methodName, $args = []){
        if(is_callable([$this, $methodName])){
            return call_user_func_array([$this,$methodName],$args);
        } else {
            throw new Except('В контроллере '.get_called_class().' метод '.$methodName.' не нейден!');
        }
    }
}