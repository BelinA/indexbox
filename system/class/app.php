<?
class App extends Single{

	public function __construct(){
		//$this->db = new dataBase();
	}

    function start(){

        Router::getI()->parse();

        $controller = ucfirst(Router::getI()->controller).'Controller';
        $action = 'action'.Router::getI()->action;

        //echo $controller.','.$action.'<br>';

        if(method_exists($controller, $action)) {
            $controller = new $controller;
            $controller->$action();
        } else {
            header('Location: /index.php?controller=main&action=index');
            exit;
        }
    }

}