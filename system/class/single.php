<?php

abstract class Single{

    private static $_aInstances = [];

    public static function getInstance() {
        $sClassName = get_called_class();
        if(class_exists($sClassName)){
            if(!isset(self::$_aInstances[$sClassName])){
                self::$_aInstances[$sClassName] = new $sClassName();
            }
            return self::$_aInstances[$sClassName];
        }
        return 0;
    }

    public static function getI() {
        return self::getInstance();
    }

    final private function __clone(){}
}
