<?
class dataBase {

	private static $db = null;
	private	$connect;
	public $result = false;

	private function __construct() {
        include  ROOT.'dbconfig.php';
		$this->connect = new mysqli(
            $db_server,
            $db_user_name,
            $db_user_pass,
            $db_name
        );
		if ($this->connect->connect_errno > 0) {
			exit($this->connect->connect_error);
		}
		$this->connect->set_charset("utf8");
	}

	public function query($query, $params = false) {
		$statement = $this->connect->prepare($query);
		if ($params) {
            for ($i = 0; $i < count($params); $i++)
            {
                $bind_name = 'bind'.$i;
                $$bind_name = $params[$i];
                $bind_names[] = &$$bind_name;
            }
            call_user_func_array(array($statement,'bind_param'),$bind_names);
        }
		$statement->execute();
		$statement->bind_result($result);
		$statement->fetch();
        $statement->close();
        $this->result = isset($result) ? $result : false;
        return $this->result;
	}
};