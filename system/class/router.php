<?
class Router extends Single{

    public $action = 'Index';
    public $controller = 'Main';

    function parse(){
        if(isset($_REQUEST['controller'])) {
            $this->controller = strtolower($_REQUEST['controller']);
        }

        if( isset($_REQUEST['action']) ) {
            $this->action = ucfirst(strtolower($_REQUEST['action']));
        }
    }
}