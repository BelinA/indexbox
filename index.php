<?
    session_start();
    // http://wmaster24.ru/index.html?controller=main&action=index
    if (is_file('config.php')) {
        require_once('config.php');
    } else {
        exit ('Не найден файл конфигурации.');
    }

    if (is_file(SYS.'fwork.php')) {
        include SYS.'fwork.php';
    } else {
        exit ('Не найден файл фрэймворка.');
    }

    App::getI()->start();
